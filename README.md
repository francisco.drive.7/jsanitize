[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]

Java Sanitizer

A ``` javax.validation.constraint``` implementation for a input sanitizer`

## License

Distributed under the MIT License. See `LICENSE` for more information.

## Contact

Francisco Martins - [@twitter_handle](https://twitter.com/0xfcmartins) - info@francisco-martins.pt

## Acknowledgements
[forks-url]: https://github.com/0xfcmartins/jsanitize/network/members
[stars-url]: https://github.com/0xfcmartins/jsanitize/stargazers
[issues-url]: https://github.com/0xfcmartins/jsanitize/issues
[license-url]: https://github.com/0xfcmartins/jsanitize/blob/master/LICENSE.txt
[issues-shield]: https://img.shields.io/github/issues/0xfcmartins/jsanitize.svg?style=for-the-badge
[forks-shield]: https://img.shields.io/github/forks/0xfcmartins/jsanitize.svg?style=for-the-badge
[stars-shield]: https://img.shields.io/github/stars/0xfcmartins/jsanitize.svg?style=for-the-badge
[license-shield]: https://img.shields.io/github/license/0xfcmartins/jsanitize.svg?style=for-the-badge
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[contributors-shield]: https://img.shields.io/github/contributors/0xfcmartins/jsanitize.svg?style=for-the-badge

